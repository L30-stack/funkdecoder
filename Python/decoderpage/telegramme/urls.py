from django.urls import path
from .views import TelegrammeHome
from .views import TelegrammeDetail

urlpatterns = [
     path('', TelegrammeHome.as_view(), name='telegramme_start'),
     path('<int:pk>/', TelegrammeDetail.as_view(), name='telegramme_detail'),
]
