import logging
import helper.config
conf = helper.config.initconfig()

def filter(decoded):
    try:
        if "POCSAG" in decoded['protocol']:
            if decoded['type'] == "network" and conf['filternetwork'] == "True":
                logging.debug("Filter network %s", decoded)
            elif decoded['encoded'] and conf['filterencoded'] == "True":
                logging.debug("Filter encoded %s", decoded)
            elif decoded['nomsg'] and conf['filternomsg'] == "True":
                logging.debug("Filter nomsg %s", decoded)
            elif len(decoded['msg'])<10 and conf['filtermsglen'] == "True":
                logging.debug("Filter short %s", decoded)
            else:
                return decoded
        if "APRS" in decoded['protocol']:
            return decoded
    except Exception as e:
        logging.info("Filter Plugin fehlgeschlagen: " + str(e))
        logging.info(decoded)
    return False