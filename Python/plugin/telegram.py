import telegram
import helper.config
import logging

conf = helper.config.initconfig()

def send(data):
    try:
        text = data['msg']
        bot = telegram.Bot(token=conf['telegramtoken'])
        if data['geo']:
            bot.send_location(conf['telegramchatid'], latitude=data['lat_float'], longitude=data['lon_float'])
        bot.send_message(conf['telegramchatid'], text=text)
    except Exception as e:
        logging.debug("Telegram Plugin fehlgeschlagen: " + str(e))
        logging.debug(data)
        print("Telegram Plugin fehlgeschlagen: " + str(e))
