import aprslib
import helper.config

conf = helper.config.initconfig()

def send(decoded):
    # a valid passcode for the callsign is required in order to send
    AIS = aprslib.IS(conf['aprsCALL'], passwd=conf['aprspasscode'], port=14580)
    AIS.connect()
    # send a single status message
    AIS.sendall(decoded['raw'])