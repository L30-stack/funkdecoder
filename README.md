# ADIFCheck

This Project is for decode different radio signals with an SDR stick

Its an alternative to: 
- [BOSWatch](https://github.com/Schrolli91/BOSWatch)
- [BOSMon](https://www.bosmon.de/)

Yoused Packages:
- [RTL-SDR](https://github.com/osmocom/rtl-sdr)
- [multimon-ng](https://github.com/EliasOenal/multimon-ng)

The project has actually devlopment status, you can see the status on :white_check_mark: :white_large_square:


## Functions
- Decode POCSAG Signals from german BOS :white_check_mark:
- Decode Hamradio APRS :white_check_mark:
- Decode Dapnet :white_check_mark:
- Decode ZVEI :white_large_square:
- Decode FMS :white_large_square:
- Filter the beacons :white_check_mark:
- Find coordinates from address in beacon :white_check_mark:
- Filtering doubble alarms :white_check_mark:


## Plugins
- Send Email with Baecon Messages :white_check_mark:
- Send Telegram message :white_check_mark:
- encrypt messages with PGP :white_large_square:
- Web based alarm monitor :white_large_square:
- Web based APRS map :white_large_square:
- Send APRS beacons to aprs.fi :white_large_square:
- Save to MySQL Database :white_large_square:
- Save to ArangoDB :white_large_square:
- weather altert :white_large_square:
- Send data to Divera :white_large_square:
- Send data to BosMon :white_large_square:
- Send FFAgent message :white_large_square:
- Send Pushover message :white_large_square:
- Send SMS message :white_large_square:
- IOBroker connection :white_large_square:


## Install

```bash
git clone https://gitlab.com/woehrer/funkdecoder.git
cd funkdecoder
./install.sh
```

## Start

### On Ubuntu:
```bash
./start.sh
```
# Dokumentation
## Configuration

